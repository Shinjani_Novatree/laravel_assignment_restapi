@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading" align="center"  style="background-color:lightpink"><h2>PRODUCT DETAILS</h2></div>
                      <div class="panel-body">
                        <div class="col-md-6">
                            <img src="<?php echo $final_array['path']?>" alt="<?php echo $final_array['product name']?>" width="400px">
                            <h3 style="margin-left: 80px"><b>PRICE: &nbsp;&#8377;&nbsp;<?php echo $final_array['price']?></b></h3>
                        </div>
                        <div class="col-md-6">
                         <h2 style="margin-left: 30px" class="text-uppercase"><strong><?php echo $final_array['product name']?></strong></h2>
                            <p align="justify"  style="margin-left: 30px"><b><u><?php echo $final_array['description']?></u></b></p>
                            <ul>
                                <li><strong>PRODUCT ID: </strong><b><?php echo $final_array['product id']?></b></li>
                                <li><strong>SHORT DESCRIPTION: </strong><b><?php echo $final_array['short description']?></b></li>
                                <li><strong>RATING: </strong><b><?php echo $final_array['rating']?></b></li>
                                <li><strong>MEAL: </strong><b><?php echo $final_array['meal']?></b></li>
                                <li><strong>MEAL TYPE: </strong><b><?php echo $final_array['meal type']?></b></li>
                                <li><strong>CATEGORY: </strong><b><?php echo $final_array['category']?></b></li>
                                <li><strong>FAVOURITE: </strong><b><?php echo $final_array['favourite']?></b></li>
                                <li><strong>AVAILABILITY: </strong><b><?php echo $final_array['availability']? "UNAVAILABLE": "AVAILABLE"?></li>
                            </ul>

                        </div>



                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection





