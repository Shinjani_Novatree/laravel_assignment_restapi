@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-primary">
                    <div class="panel-heading" align="center"><h4>PRODUCTS</h4></div>

                    <div class="panel-body">
                        <table class="table table-hover table-striped table-responsive" style="width:100%">
                            <col width="120">
                            <col width="120">
                            <col width="120">
                            <col width="120">
                            <tr class="info">
                                <th><center><u>Product Name</u></center></th>
                                <th><center><u>Product ID</u></center></th>
                                <th><center><u>Product Price</u></center></th>
                                <th><center><u>Image</u></center></th>
                                <th><center><u>View Details</u></center></th>
                            </tr>
                            <?php

                                foreach($data as $product)
                                    {
                                        $p_json=json_encode($product);
                            ?>
                                        <tr  style="margin-top: 50px">
                                            <td align="center"><?php echo $product['product name']?></td>
                                            <td align="center"><?php echo $product['product id']?></td>
                                            <td align="center"><?php echo $product['price']?></td>
                                            <td align="center"><img src="<?php echo $product['path']?>" alt="<?php echo $product['product name']?>" vspace="10px" width="150px"></td>
                                            <td align="center"><!--<a href="details/<?php /*echo $product['product id']*/?>">VIEW</a>-->
                                                <form action={{URL::to('details')}} method="POST">
                                                    <input type="hidden" name="data" value='<?php echo $p_json?>'> </input>
                                                    <input type="hidden" name="_token" value='{{csrf_token()}}'></input>
                                                    <input type="submit" value="View Product Details" style="margin-top: 50px" class="btn btn-success">
                                                </form>
                                            </td>
                                        </tr>
                            <?php
                                    }
                            ?>


                        </table>
                    </div>
               <?php if (isset($pcount)){ ?>
                    <div class="col-md-12 text-center">
                    <ul class="pagination pagination-lg pager" id="myPager">
                    <?php
                    for($i=1;$i<=$pcount;$i++){
                    ?>
                    <li><a href="{{URL::to('list')}}/<?php echo $i ?>"><?php echo $i ?></a></li>
                    <?php
                    }
                    ?>
                    </ul>
                </div>
                <?php
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>
@endsection





