<?php


Route::get('/', function () {
    return view('welcome');
});


/*The routes are defined here without the need of user login*/

        /*  Route::get('product',array('uses'=> 'TestController@getSession' , 'as'=> 'product'));
          Route::get('details',['uses'=>'TestController@getDetails']);
          Route::post('details',['uses'=>'TestController@getDetails']);
          Route::get('list/{limit}','TestController@getPage');
          Route::get('list',['uses'=>'TestController@pagination']);*/

/*This route is for the alternate drupal view provided*/

      /*Route::get('details/{data}',['uses'=>'TestController1@getDetails']);*/

Route::group(['middleware' => 'web'], function () {

  /*The routes are defined here with the need of user login*/
    Route::auth();

    Route::get('/home', 'HomeController@index');
    Route::get('product',array('uses'=> 'TestController@getSession' , 'as'=> 'product'));
    Route::get('details',['uses'=>'TestController@getDetails']);
    Route::post('details',['uses'=>'TestController@getDetails']);
    Route::get('list/{limit}',['uses'=> 'TestController@getPage']);
    Route::get('list',['uses'=>'TestController@pagination']);

});

/*product -> for the products list
get details -> for UX of product details
post details -> for viewing each product details
list/{limit}-> for paginated view of product list
list-> for redirecting automatically to the 1st page of pagination
*/
