<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Control;


class TestController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth');
    }

    public function getSession()
    {
        /*passing the url for unauthenticated token by curl post*/
        $url1 = "http://103.3.63.105/freshtopia/rest/user/token";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url1);
        curl_setopt($curl, CURLOPT_POST, 1); // Do a regular HTTP POST
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        $unauth = curl_exec($curl);/*got the json encoded unauthenticated token*/
        $array = json_decode($unauth, true);/*the json encoded token is converted to array*/


        /*the username and passwords are sent as an array*/
        $post_field = array(
            "username" => "admin",
            "password" => "admin"
        );

        /*the user details array is json encoded to be passed into postfields of curl post function*/
        $js = json_encode($post_field);

        $header = 'X-CSRF-Token : $array[\'token\']';/*passing the token in through a header*/
        $url2 = "http://103.3.63.105/freshtopia/rest/user/login";
        /*this curl post function is for getting the authenticated token with proper user login*/
        $curls = curl_init();
        curl_setopt($curls, CURLOPT_URL, $url2);
        curl_setopt($curls, CURLOPT_POST, 1); // Do a regular HTTP POST
        curl_setopt($curls, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $header));
        curl_setopt($curls, CURLOPT_POSTFIELDS, $js); // Set POST data
        curl_setopt($curls, CURLOPT_RETURNTRANSFER, TRUE);
        $response = curl_exec($curls);/*json encoded authenticated token*/

        $arr = json_decode($response, true); /*authenticated token decoded to array*/

        $header = 'X-CSRF-Token : ' . $arr['token'];/*passing the token into header*/


        $cookie_session = $arr['session_name'] . '=' . $arr['sessid']; /*sending the session details for curl get function*/

        $url3 = "http://103.3.63.105/freshtopia/rest/views/product_list";/*view from which data is to be fetched*/

        $curl_get = curl_init();
        curl_setopt($curl_get, CURLOPT_URL, $url3);
        curl_setopt($curl_get, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $header));
        curl_setopt($curl_get, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl_get, CURLOPT_COOKIE, $cookie_session);
        $execute = curl_exec($curl_get);/*json encoded data*/
        curl_close($curl_get);
        $array1 = json_decode($execute, true); /*array format data*/

        return view('pdt_list')->with('data', $array1);

    }

    public function getPage($limit)
    {
/*this function is used for paginated view of the products list*/

        $url1 = "http://103.3.63.105/freshtopia/rest/user/token";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url1);
        curl_setopt($curl, CURLOPT_POST, 1); // Do a regular HTTP POST
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        $unauth = curl_exec($curl);
        $array = json_decode($unauth, true);

        $post_field = array(
            "username" => "admin",
            "password" => "admin"
        );

        $js = json_encode($post_field);

        $header = 'X-CSRF-Token : $array[\'token\']';
        $url2 = "http://103.3.63.105/freshtopia/rest/user/login";

        $curls = curl_init();
        curl_setopt($curls, CURLOPT_URL, $url2);
        curl_setopt($curls, CURLOPT_POST, 1); // Do a regular HTTP POST
        curl_setopt($curls, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $header));
        curl_setopt($curls, CURLOPT_POSTFIELDS, $js); // Set POST data
        curl_setopt($curls, CURLOPT_RETURNTRANSFER, TRUE);
        $response = curl_exec($curls);

        $arr = json_decode($response, true);


        $header = 'X-CSRF-Token : ' . $arr['token'];


        $cookie_session = $arr['session_name'] . '=' . $arr['sessid'];

        $url3 = "http://103.3.63.105/freshtopia/rest/views/product_list";

        $curl_get = curl_init();
        curl_setopt($curl_get, CURLOPT_URL, $url3);
        curl_setopt($curl_get, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $header));
        curl_setopt($curl_get, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl_get, CURLOPT_COOKIE, $cookie_session);
        $execute = curl_exec($curl_get);
        curl_close($curl_get);
        $array1 = json_decode($execute, true);

        for($i = ($limit-1)*2; $i<($limit)*2; $i++){
            $list_n[]=$array1[$i];
        }
        $total=count($array1);
        $page=$total / 2;



        return view('pdt_list')->with('data', $list_n)->with('pcount',$page);

    }
    public function pagination()
    {
/*this function is for get list url.. it by default redirects to list/1 from list*/
     $limit=1;
        $url1 = "http://103.3.63.105/freshtopia/rest/user/token";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url1);
        curl_setopt($curl, CURLOPT_POST, 1); // Do a regular HTTP POST
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        $unauth = curl_exec($curl);
        $array = json_decode($unauth, true);

        $post_field = array(
            "username" => "admin",
            "password" => "admin"
        );

        $js = json_encode($post_field);

        $header = 'X-CSRF-Token : $array[\'token\']';
        $url2 = "http://103.3.63.105/freshtopia/rest/user/login";

        $curls = curl_init();
        curl_setopt($curls, CURLOPT_URL, $url2);
        curl_setopt($curls, CURLOPT_POST, 1); // Do a regular HTTP POST
        curl_setopt($curls, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $header));
        curl_setopt($curls, CURLOPT_POSTFIELDS, $js); // Set POST data
        curl_setopt($curls, CURLOPT_RETURNTRANSFER, TRUE);
        $response = curl_exec($curls);

        $arr = json_decode($response, true);


        $header = 'X-CSRF-Token : ' . $arr['token'];


        $cookie_session = $arr['session_name'] . '=' . $arr['sessid'];

        $url3 = "http://103.3.63.105/freshtopia/rest/views/product_list";

        $curl_get = curl_init();
        curl_setopt($curl_get, CURLOPT_URL, $url3);
        curl_setopt($curl_get, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $header));
        curl_setopt($curl_get, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl_get, CURLOPT_COOKIE, $cookie_session);
        $execute = curl_exec($curl_get);
        curl_close($curl_get);
        $array1 = json_decode($execute, true);

        for($i = ($limit-1)*2; $i<($limit)*2; $i++){
            $list_n[]=$array1[$i];
        }
        $total=count($array1);
        $page=$total / 2;



        return view('pdt_list')->with('data', $list_n)->with('pcount',$page);





    }

    public function getDetails()
    {
        if($_POST!=null){
            $p_details_json=$_POST['data'];
            $p_details = json_decode($p_details_json , true);
            return view('pdt_details')->with('final_array', $p_details);
        }
        else
            return redirect()->route('product');

    }

}


