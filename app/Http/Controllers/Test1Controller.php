<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Control;


class Test1Controller extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function getDetails($id)
    {
        $url1 = "http://103.3.63.105/freshtopia/rest/user/token";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url1);
        curl_setopt($curl, CURLOPT_POST, 1); // Do a regular HTTP POST
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        $unauth = curl_exec($curl);
        $array= json_decode($unauth , true);

        $post_field = array(
            "username" => "admin",
            "password" => "admin"
        );

        $js=json_encode($post_field);

        $header = 'X-CSRF-Token : $array[\'token\']';
        $url2 = "http://103.3.63.105/freshtopia/rest/user/login";

        $curls = curl_init();
        curl_setopt($curls, CURLOPT_URL, $url2);
        curl_setopt($curls, CURLOPT_POST, 1); // Do a regular HTTP POST
        curl_setopt($curls, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $header));
        curl_setopt($curls, CURLOPT_POSTFIELDS, $js); // Set POST data
        curl_setopt($curls, CURLOPT_RETURNTRANSFER, TRUE);
        $response = curl_exec($curls);

        $arr= json_decode($response , true);

        /* $key= array_keys($response);*/


        $header='X-CSRF-Token : '. $arr['token'];


        $cookie_session=  $arr['session_name'].'=' .$arr['sessid'];

        $url3 = "http://103.3.63.105/freshtopia/rest/views/product_page?args[0]=$id";

        $curl_get = curl_init();
        curl_setopt($curl_get, CURLOPT_URL, $url3);
        curl_setopt($curl_get, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $header));
        curl_setopt($curl_get, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl_get, CURLOPT_COOKIE, $cookie_session);
        $execute = curl_exec($curl_get);
        curl_close($curl_get);
        $array1=json_decode($execute , true);


        return view('pdt_details')->with('data', $array1);

    }



}
